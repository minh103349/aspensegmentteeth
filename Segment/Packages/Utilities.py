import logging
import sys
import os


def init_logger(log_dir, out_dir):
    if not os.path.exists(log_dir):
        log_dir = out_dir
        logging.info("Output directory is used for log")
    """
        check if log_dir exists
    """
    logging.debug(log_dir)
    log_location = os.path.join(log_dir, 'log.txt')
    logging.debug(log_location)

    logging.basicConfig(filename=log_location, level=logging.DEBUG, format='%(asctime)s:%(message)s', filemode='w')
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
    print('utlities is imported')
    pass

# todo moving path checker here

def path_check(path):
    if not os.path.exists(path):
        error = (f"Path does not exists: {path}")
        logging.error(error)
        raise Exception(error)