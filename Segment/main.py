import argparse
import logging
import os
from Packages import Utilities

parser = argparse.ArgumentParser()
parser.add_argument('--inputdirectory', type=str, required=True, help='Please input the source directory')
parser.add_argument('--outputdirectory', type=str, required=True, help='Please input the directory where files are saved')
parser.add_argument('--logdirectory', '-l', type=str, required=False, default='')
args = parser.parse_args()

Utilities.path_check(args.inputdirectory)
Utilities.path_check(args.outputdirectory)
"""
before initating logger, I want you to check if the user provided -logdirectory. if not than use -outputdirectory
"""


Utilities.init_logger(args.logdirectory, args.outputdirectory)
logging.debug('log this message')



# check inputdirectory and outputdirectory only

"""
if os.path.exists(args.inputdirectory) == False or os.path.exists(args.outputdirectory) == False or os.path.exists(args.logdirectory) == False:
    print("Please input a valid directoy")
if os.path.exists(args.inputdirectory) == True and os.path.exists(args.outputdirectory) == True and os.path.exists(args.logdirectory) == True:
    print(args.inputdirectory + '\n' + args.outputdirectory + '\n' + args.logdirectory)
"""

"""
  todo get a list of paths for files in input directory
  os.walk()
"""

