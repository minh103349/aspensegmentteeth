from boxsdk import OAuth2, Client, exception
from packages import utils, apiCalls
import logging
import os


def get_client():
    oauth = OAuth2(
        client_id='hw785wawyeaifvk6zjgsogxcivocht0h',
        client_secret='k6wXq450Zfyv21oEsCgeS0m3gNPvEumc',
        access_token='VN33RXFYX1Wmn3vIJglWruZk1lGigCk3'
    )
    return Client(oauth)


def case_info(case_list, ccfs_connect):
    uid_list = utils.get_cases(case_list)
    cases = []
    for c in uid_list:
        cases.append(ccfs_connect.download_case(c))
    logging.info('Cases loaded from boxCastList.txt')
    return cases


def upload_files(cases, box_folder):
    error_cases = []
    client = get_client()
    box_folder = client.folder(box_folder)
    for c in cases:
        logging.info(f'Starting case: {c}')
        folder = c['folder'].replace('\\', '/')
        case_name = c['geocx']['Case']['Reference']

        try:
            logging.info(f'Making box folder for: {case_name}')
            subfolder = box_folder.create_subfolder(case_name)
            for root, dirs, files in os.walk(os.path.join(folder, 'export')):
                for f in files:
                    if not f.endswith('.zip'):
                        print(f'Uploading: {f} for case: {case_name}')
                        subfolder.upload(os.path.join(root, f), f)
                        print('File upload complete.')
        except exception.BoxAPIException as e:
            logging.error(f'Unable to make folder or copy file for case. '
                          f'Make sure case or folder does not exist in box already: '
                          f'{c}\nException:{e}')
            error_cases.append(c)
        return error_cases


def report(errors):
    logging.info('FINAL REPORT')
    if errors:
        logging.info(f'Total case Errors: {len(errors)}')
        logging.info('Cases that did not upload properly:')
        for e in errors:
            logging.info(e)
        logging.info('END OF LIST')
    else:
        logging.info('No errors found, all cases uploaded')


if __name__ == '__main__':
    settings = utils.load_settings()

    log_dir = os.path.join(os.getcwd(), 'log_dir')
    utils.init_logger(log_dir, settings['log_level'])

    ccfs_connect = apiCalls.Connection(**settings)
    cases = case_info(settings['case_list_box'], ccfs_connect)
    errors = upload_files(cases, settings['box_folder'])
    report(errors)



