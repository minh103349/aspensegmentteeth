import requests
from time import sleep
import platform
import logging

status_cycles = 60  # used to timeout 1 seconds and call download status every time


class Connection:
    def __init__(self, **kwargs):
        self.email = kwargs['ccfs_email']
        self.password = kwargs['ccfs_pass']
        self.ccfs_url = kwargs['ccfs_url']
        self.ums_url = kwargs['ums_url']
        self.cms_url = kwargs['cms_url']
        self._token = ''  # Do not access class token directly use Connection.get_token()
        self.set_token()
        self.headers = {'X-GL-Authorization': f'GLToken {self.get_token()}',
                        'X-GL-UserName': self.email,
                        'X-GL-CmsUrl': self.cms_url}

    def get_resp(self, method, url, headers={}, body={}):
        resp = None
        try:
            if method == 'post':
                resp = requests.post(url,
                                     headers=headers,
                                     json=body,
                                     verify=False)
            if method == 'get':
                resp = requests.get(url,
                                    headers=headers,
                                    verify=False)

        except Exception as e:
            logging.exception(e)
            logging.exception('Make sure you are connected to Work network before running script')
            raise Exception(e)
        return resp

    def set_token(self):
        url = self.ums_url + 'ums/api/v1/users/auth'
        body = {"email": self.email, "password": self.password}

        resp = self.get_resp('post', url, body=body)
        if resp.status_code != 200:
            msg = f'Unable to authenticate. \n\tURL:{url}\n\tStatus Code: {resp.status_code}'
            logging.exception(msg)
            raise Exception(msg)
        logging.info('Status code: 200')
        self._token = resp.json()['token']['id']
        logging.info(f'New Token received')

    def get_token(self):
        url = 'ums/api/v1/users/verifyToken'
        url = self.ums_url + url
        headers = {'X-GL-Authorization': f'GLToken {self._token}',
                   'X-GL-UserName': self.email}
        resp = requests.post(url, headers=headers, verify=False)
        tries = 0
        while tries <= 5:
            if resp.status_code == 200:
                logging.info('Token still good to go.')
                break
            else:
                tries += 1
                logging.info(f'Re-authenticating token. Number of tries: {tries}')
                self.set_token()
                resp = requests.post(url, headers=headers, verify=False)
        return self._token

    def download_case(self, caseUID):
        logging.info(f'Download case started for: {caseUID}')
        url = f'{self.ccfs_url}/ccfs/api/v4/cases/get?caseUid={caseUID}&noWait=true'
        resp = self.get_resp('get', url, headers=self.headers)
        logging.info(f'Download case status code: {resp.status_code}')

        if resp.status_code == 200:
            logging.info(f'Case downloaded and ready for processing: {caseUID}')
            return resp.json()
        if resp.status_code != 400:
            return {'script_error': f'Download case returned error: {resp.status_code}\nERROR: {resp.json()["error"]}'}
        case_error = resp.json()['error']
        if case_error:
            logging.error(case_error)
            return {'script_error': f'Download case error: {case_error}'}
        status = self.download_status(caseUID)
        if status:
            resp = requests.get(url, headers=self.headers, verify=False)
            if resp.status_code == 200:
                return resp.json()
            else:
                return {'script_error': f'Download status showed complete but cannot get status 200 when downloading case again.'}
        else:
            return {'script_error': 'Error with checking download complete'}

    def download_status(self, caseUID):
        url = f'{self.ccfs_url}/ccfs/api/v4/cases/getDownloadStateSummary?caseID={caseUID}'
        count = 0
        while count < status_cycles:
            sleep(1)
            resp = self.get_resp('get', url, headers=self.headers)
            if resp.json()['transferFinished']:
                return True
            if resp.status_code != 200:
                logging.error(f'Something went wrong checking download progress.\nCASE UID: {caseUID}\nStatus Code: {resp.status_code}')
                return False
            percent_transfered = resp.json()['transferSizeCompleted'] / resp.json()['transferSize']
            logging.debug('Checked download progress:\n\tCaseUID: {}\n\tPercent Complete: {:.0%}'.format(caseUID, percent_transfered))
            count += 1
            if count >= status_cycles:
                logging.error(
                    f'Download case is taking too long. Check connection or increase try counter. Case: {caseUID}')
                return False

    def lock_case(self, caseUID):
        url = f'{self.ccfs_url}/ccfs/api/v4/cases/lock'
        json_body = {'caseUid': caseUID,
                     'machine': platform.node(),
                     'client': 'Python script for creating printable models'
                     }
        resp = requests.post(url, headers=self.headers, json=json_body, verify=False)
        if resp.status_code != 200:
            logging.error(f'Unable to lock case: {caseUID} . Status code: {resp.status_code}\n\tCheck case is not already locked on CCFS')
            return {'script_error': 'Unable to lock case'}
        logging.info(f'Case locked: {caseUID}')
        return ''

    def update_case(self, caseUID, case_info):
        logging.info('Updating CCFS for case')
        body = case_info
        body['unlock'] = True
        body['machine'] = platform.node()
        body['client'] = 'Python script for creating printable models'
        url = f'{self.ccfs_url}/ccfs/api/v4/cases/update'

        resp = requests.post(url, headers=self.headers, json=body, verify=False)
        if resp.status_code != 200:
            logging.error(f'Unable to update this case: {caseUID}\n\tResponse:{resp.json()}')
            return False
        logging.info(f'Successfully updated case: {caseUID}')
        return True


