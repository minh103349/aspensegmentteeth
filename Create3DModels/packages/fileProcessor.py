from . import utils
import threading
import queue
import logging
import zipfile
import os
from subprocess import Popen, PIPE

download_complete = False
all_cases = []
q = queue.Queue()


class Case:
    def __init__(self, caseUID, case_info):
        self.caseUID = caseUID
        self.case_info = case_info
        self.script_path = None
        self.error = []

    def create_pyscript(self, base_height, log_dir, bite_align=False):
        case_folder = self.case_info['folder'].replace('\\', '/')
        logging.info(f'Case Folder: {case_folder}')

        lower_path = f"{case_folder}/export/lower articulated.stl"
        upper_path = f"{case_folder}/export/upper articulated.stl"

        if not os.path.exists(lower_path) or not os.path.exists(upper_path):
            self.error.append('"upper articulated.stl" or "lower articulated.stl" does not exist in case export folder')

        aspen_template = f'''
# -*- coding: utf-8 -*-
import os
geo.new()

# Open and prep upper model
geo.open_safe(is_import=0, files=["{upper_path}"])
objU = geo.get_objects(name="upper articulated")[0]
objU.active = True
objU.name = 'Scan Data Upper'
num_tri, max_bbox_len, avg_edge_len, area =geo.get_mesh_stats(objU.model)
if max_bbox_len > 0.300:
    geo.scale_by(objU, .001)
geo.convert_in_jaw( occ_dir = [0., -1., 0.] )
geo.detect_teeth(fill_holes_method=2, use_combined=False, min_delete_dist_from_bd=0)
geo.select_objects(object_names=[])

# Open and prep lower model
geo.open_safe(is_import=1, files=["{lower_path}"])
objL = geo.get_objects(name="lower articulated")[0]
objL.active = True
objL.name = 'Scan Data Lower'
num_tri, max_bbox_len, avg_edge_len, area =geo.get_mesh_stats(objL.model)
if max_bbox_len > 0.300:
    geo.scale_by(objL, .001)
geo.convert_in_jaw( occ_dir = [0., 1., 0.] )
geo.detect_teeth(fill_holes_method=2, use_combined=False, min_delete_dist_from_bd=0)
geo.select_objects(object_names=[])

# Processes for bite align and printable models
geo.select_objects(object_names=['Scan Data Upper', 'Scan Data Lower'])

if {bite_align}:
    geo.ct_adjust_bite()
geo.biotemp_model(CreateTag=False, MakeScanFixtureHoles=False, UsePreparationJawOnly=False, ExportPath=u"", LowerOcclusal="(0,1,0)", UpperOcclusal="(0,-1,0)", LowerBuccal="(0,0,-1)", UpperBuccal="(0,0,-1)")

#image capture
screenshot_folder = os.path.join('{case_folder}', 'Screenshots')
if not os.path.exists(screenshot_folder):
    os.mkdir(screenshot_folder)

geo.fit_scene_to_view()

# inverted back and front view due to input model orientation
geo.front_view()
geo.screen_capture_views(os.path.join(screenshot_folder, 'backView.png'))

geo.back_view()
geo.screen_capture_views(os.path.join(screenshot_folder, 'frontView.png'))

geo.left_view()
geo.screen_capture_views(os.path.join(screenshot_folder, 'leftView.png'))

geo.right_view()
geo.screen_capture_views(os.path.join(screenshot_folder, 'rightView.png'))

geo.top_view()
geo.screen_capture_views(os.path.join(screenshot_folder, 'topView.png'))

geo.bottom_view()
geo.screen_capture_views(os.path.join(screenshot_folder, 'bottomView.png'))

printables_folder = os.path.join('{case_folder}', 'export')
if not os.path.exists(printables_folder):
    os.mkdir(printables_folder)

geo.select_objects(object_names=[])

geo.select_objects(object_names=['Printable_model_upper'])
geo.reorient_model()
geo.save_object(file_name=os.path.join(printables_folder, 'upperModel.stl'))
geo.select_objects(object_names=[])

geo.select_objects(object_names=['Printable_model_lower'])
geo.reorient_model()
geo.save_object(file_name=os.path.join(printables_folder, 'lowerModel.stl'))

    '''
        script_path = os.path.join(log_dir, self.caseUID + '.py')
        with open(script_path, 'w') as script:
            script.write(aspen_template)
        if os.path.exists(script_path):
            logging.info(f'Pyscript created: ScriptPath: {script_path}')
            self.script_path = script_path
        else:
            msg = f'Pyscript was not created, check if {log_dir} exists'
            logging.error(msg)
            self.error.append(msg)

    def process_case(self, exe, connection, show_ui):
        if not os.path.exists(exe):
            logging.error(f'Path to executable does not exist: {exe}')
        flags = [
            exe,
            self.script_path,
            '-disableOptix',
            '-ExitOnMacroEnd',
            '-noPopUps',
            '-noSplash',
            '-developer'
        ]
        logging.info(f'Running executable on: \n\tCASE: {self.caseUID}')
        logging.debug(f'CMD ARGS: {flags}')
        if not show_ui:
            flags.append('-noShow')
        proc = Popen(flags, stdout=PIPE, stderr=PIPE)
        stdout, stderr = proc.communicate()

        if 'Script Test Passed' in str(stdout):
            logging.info(f'Executable ran successfully for CASE: {self.caseUID}')

            # adds file to existing zip in case folder
            case_folder = self.case_info['folder'].replace('\\', '/')
            lower_path = f"{case_folder}/export/lowerModel.stl"
            upper_path = f"{case_folder}/export/upperModel.stl"
            existing_zip = os.path.join(case_folder, 'export/exports.zip')
            with zipfile.ZipFile(existing_zip, 'a') as zipf:
                zipf.write(lower_path, os.path.basename(lower_path))
                zipf.write(upper_path, os.path.basename(upper_path))
        else:
            msg = f'Executable failed to process case: {self.caseUID}\n\tSTDOUT: {stdout}\n\tCheck models manually.'
            logging.error(msg)
            self.error.append(msg)
        success = connection.update_case(self.caseUID, self.case_info)
        if not success:
            msg = 'Unable to properly lock case'
            self.error.append(msg)
            logging.error(msg)


def process(exe, show_ui, connection, base_height, log_dir, bite_align):
    while not download_complete or not q.empty():
        case = q.get()
        result = connection.lock_case(case.caseUID)
        if 'script_error' in result:
            case.error.append(result['script_error'])
        case.create_pyscript(base_height, log_dir, bite_align)
        if not case.error:
            case.process_case(exe, connection, show_ui)


def download_cases(case_list, connection):

    for caseUID in case_list:
        logging.info(f'Starting download for case: {caseUID}')
        result = connection.download_case(caseUID)
        case = Case(caseUID, result)
        if 'script_error' in result:
            case.error.append(result['script_error'])
        else:
            q.put(case)
        all_cases.append(case)
    global download_complete
    download_complete = True


def start(connection, log_dir, **kwargs):
    case_list = utils.get_cases(kwargs['case_list'])

    download = threading.Thread(target=download_cases,
                                args=(case_list,
                                      connection)
                                )
    process_cases = threading.Thread(target=process,
                                     args=(
                                         kwargs['aspen_exe'],
                                         kwargs['show_ui'],
                                         connection,
                                         kwargs['base_height'],
                                         log_dir,
                                         kwargs['bite_align'])
                                     )

    # This fixes hanging when trying to run one case and it fails download.
    # 'else' statement allows first thread to complete and doesn't multithread
    if len(case_list) >= 5:
        logging.info('Multithreading used.')
        download.start()
        process_cases.start()
        download.join()
        process_cases.join()
    else:
        logging.info('Multithreading not used. Less than 5 cases listed.')
        download.start()
        download.join()
        process_cases.start()
        process_cases.join()

    utils.report(all_cases, kwargs['case_list'])
