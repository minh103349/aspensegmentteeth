import os
import stat
import sys
import json
import logging


def init_logger(log_dir, log_level):
    numeric_level = getattr(logging, log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % log_level)
    log_path = os.path.join(log_dir, (os.path.basename(sys.argv[0]).split('.')[0] + '_outputLogs.txt'))

    log_format = '%(levelname)s: %(message)s'
    if log_level == 'debug':
        log_format = '%(asctime)s.%(msecs)03d %(levelname)s {%(module)s} [%(funcName)s]: %(message)s'

    logging.basicConfig(filename=log_path,
                        level=numeric_level,
                        filemode='w',
                        format=log_format,
                        datefmt='%Y-%m-%d,%H:%M:%S')
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
    logging.info(f'Logger Path: {log_path}')
    logging.info(f'Log Format: "{log_format}"')


def get_cases(file_path):
    cases = []
    if not os.path.exists(file_path):
        msg = f'File path to case list does not exist: {file_path}'
        logging.exception(msg)
        raise Exception(msg)
    with open(file_path, 'r') as file:
        lines = [c.strip('\n') for c in file.readlines()]
        for line in lines:
            if not line.startswith('#') and line:
                cases.append(line.split('/')[-1])
        if not cases:
            msg = f"Check {file_path} and make sure it's properly formatted and " \
                  f"follows original template or list is not empty"
            logging.exception(msg)
            raise Exception(msg)
        return cases


def load_settings():
    fp = os.path.join(os.getcwd(), 'settings.json')
    if os.path.exists(fp):
        with open(fp) as json_file:
            return json.load(json_file)


def report(all_cases, file_path):
    logging.info(f'---------------------------End of Script Summary--------------------------------')
    error_cases = [c for c in all_cases if c.error]
    completed_cases = [c for c in all_cases if not c.error]

    logging.info(f'Total cases: {len(all_cases)}')
    if error_cases:
        logging.info(f'Total Error Cases: {len(error_cases)}')
        logging.info('Error Cases:')
        logging.info('{CaseUUID}: {ERROR MESSAGE}')
        for c in error_cases:
            logging.info(f'{c.caseUID}: {c.error}')
    else:
        logging.info('All cases processed with no detected errors.')

    mark_list(completed_cases, file_path)


def mark_list(completed_cases, file_path):
    os.chmod(file_path, stat.S_IWRITE)
    new_lines = []
    with open(file_path, 'r') as f:
        for line in f.readlines():
            for c in completed_cases:
                if c.caseUID in line:
                    line = '# ' + line
            new_lines.append(line)

    with open(file_path, 'w') as f:
        f.writelines(new_lines)
