import os
import shutil
from packages import apiCalls, fileProcessor, utils


'''
Requirements:
    -Python 3.8
    -MainDentalAspen 10.100.21251.2 or greater
    -Cases with upper and lower models.
    -Cases need to be unlocked on console CloudPoint
    -caseList.txt needs to be a list of case links (add '#' in front of a line to omit in script)
    -Run on computer connected to campus network for fastest results
    -Snacks (depending on how many cases are processed)

Defaults set:
    --case_list     Script looks for caseList.txt in root script folder.
    --env           QA environment
    --log_level     INFO
    --base_height   for 3d printable model is .002 meters or 2 millimeters
    --no_show       is disable which means it'll show Wave. add '-no_show' flag to not show Wave

How it Works:
    -Authenticates user/pass for ccfs and gets access token
    -Every api call verifies token before proceeding to make sure valid
    -Parses caseList.txt and extracts caseUID from links provided
    -Makes calls to CCFS and initiates downloads to CCFS shared drive, similar to how FastDesign works
    -File path to case is extracted and from geocx file
    -Pyscript is created from template. Template includes making models, scaling if needed, screenshots, and saving back to folder.
    - api called to lock case
    -Wave is ran as an individual process every case
    -api to unlock and upload case to cloud is called
    -Reported is appended to the ./log_dir/outputLogs.txt 


'''

settings = utils.load_settings()

log_dir = os.path.join(os.getcwd(), 'log_dir')
if os.path.exists(log_dir):
    shutil.rmtree(log_dir)
os.mkdir(log_dir)
utils.init_logger(log_dir, settings['log_level'])

connection = apiCalls.Connection(**settings)

fileProcessor.start(connection, log_dir, **settings)
