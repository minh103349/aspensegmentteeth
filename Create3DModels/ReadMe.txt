Requirements:
    -Python 3.8
    -Wave 11.4 or greater. If issues are found with Wave than we'll need to port PrintableModelsCreate changes to 11.3 
    -https://cbs.cloudpoint.com/cbs/api/v1/productbuild/download?id=911754df-41dc-4993-aa65-f5378336bf6a&product=GDPMain-12.0.22252.0
    -Cases with upper and lower models.
    -Cases need to be unlocked on console CloudPoint
    -caseList.txt needs to be a list of case links (add '#' in front of a line to omit in script)
    -Best if ran locally on work network

Defaults set:
    --case_list     Script looks for caseList.txt in root script folder.
    --env           QA environment
    --log_level     INFO
    --base_height   for 3d printable model is .002 meters or 2 millimeters
    --no_show       is disable which means it'll show Wave. add '-no_show' flag to not show Wave

How it Works:
    -Authenticates user/pass for ccfs and gets access token
    -Every api call verifies token before proceeding to make sure valid
    -Parses caseList.txt and extracts caseUID from links provided
    -Makes calls to CCFS and initiates downloads to CCFS shared drive, similar to how FastDesign works
    -File path to case is extracted and from geocx file
    -Pyscript is created from template. Template includes making models, scaling if needed, screenshots, and saving back to folder.
    - api called to lock case
    -Wave is ran as an individual process every case
    -api to unlock and upload case to cloud is called
    -Reported is appended to the ./log_dir/outputLogs.txt
    -Script is designed to raise exception on critical issues and stop
    -Script is designed to log errors in cases and continue



To start:
1) Open run.bat file with a text editor and provide your user/pass for CCFS. Make sure your user/pass matches environment you'll be accessing these cases
2) Save file
3) In the root folder, locate the the caseList.txt. Paste case links there. One per line. Save file. 
4) Run run.bat file
5) Once complete locate the /log_dir/outputLogs.txt to view script summary.

someone somewhere
if add more
I'm actually adding more in here